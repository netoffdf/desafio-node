/* eslint-disable no-underscore-dangle */
const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const { secret } = require('../config');

const userSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  senha: {
    type: String,
    required: true,
  },
  telefones: [{
    numero: String,
    ddd: String,
  }],
  ultimo_login: {
    type: Date,
  },
  salt: {
    type: String,
  },
  token: {
    type: String,
  },

}, { timestamps: { createdAt: 'data_criacao', updatedAt: 'data_atualizacao' } });

userSchema.pre('save', function (next) {
  if (this.isModified('senha') || this.isNew) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.senha = crypto.pbkdf2Sync(this.senha, this.salt, 10000, 512, 'sha512').toString('hex');
  }
  return next();
});

userSchema.methods.validPassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.senha === hash;
};

userSchema.methods.generateJWT = function () {
  const today = new Date();
  const exp = new Date(today);
  exp.setTime(today.getTime() + (30 * 60000));

  const token = jwt.sign({
    id: this._id,
    exp: parseInt(exp.getTime() / 1000),
  }, secret);

  this.ultimo_login = today;
  this.token = token;
  this.save();

  return token;
};

userSchema.methods.toAuthJSON = function (generateJWT = true) {
  return {
    id: this._id,
    nome: this.nome,
    email: this.email,
    token: (generateJWT) ? this.generateJWT() : this.token,
    telefones: this.telefones,
    ultimo_login: this.ultimo_login,
    data_criacao: this.data_criacao,
    data_atualizacao: this.data_atualizacao,
  };
};

const User = mongoose.model('User', userSchema);

module.exports = User;
