const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const routes = require('./routes');
const constants = require('./utils/constants');

/**
 * Load env variables.
 */
dotenv.load({ path: '.env' });

/**
 * Express.
 */
const app = express();

/**
 * Config Express
 */
app.set('host', process.env.IP || '0.0.0.0');
app.set('port', process.env.PORT || 8080);
app.use(compression());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * MongoDB.
 */
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s Erro conexão MongoDB', chalk.red('✗'));
  process.exit();
});

/**
 * Routes
 */
routes(app);

/**
 * Error Handler. e Logger
 */
if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use(errorHandler());
  app.use(logger('dev'));
}

/**
 * 404 - Not found
 */
app.use((req, res) => {
  res.status(constants.HTTP_URLNOTFOUND).json({ mensagem: 'rota não encontrada' });
});


/**
 * Start Express.
 */
app.listen(app.get('port'), () => {
  console.log('%s IP http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
});

module.exports = app;
