const { expect } = require('chai');
const chai = require('chai');

const chaiHttp = require('chai-http');
const app = require('../app');
const User = require('../models/User');
const constants = require('../utils/constants');

const should = chai.should();

chai.use(chaiHttp);
let user;

describe('CADASTRO, LOGIN e TOKEN JWT', () => {

  describe('Create Default User and Check 404', () => {
    it('Clean DB Create Default User', (done) => {
      User.remove({}, (err) => {
        User.create({
          nome: 'Francisco Dias',
          email: 'fcodias@gmail.com',
          senha: '123mudar',
          telefones: [{ numero: 999999999, ddd: 99 }],
        }).then((userResponse) => {
          user = userResponse.toAuthJSON();

          userResponse.validate((err) => {
            expect(err).to.not.exist;
            done();
          });
        });
      });
    });

    it('404 Rota não encontrada', (done) => {
      chai.request(app)
        .get('/naoexiste')
        .set('content-type', 'application/json')
        .end((err, res) => {
          expect(res).to.have.status(constants.HTTP_URLNOTFOUND);
          done();
        });
    });
  });

  describe('Cadastro - POST /users', () => {
    it('Faltando campo nome - POST /users', (done) => {
      chai.request(app)
        .post('/users')
        .send({
          email: 'netoffdf@gmail.com',
          senha: '123456',
          telefones: [{ numero: 999999, ddd: 81 }],
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.to.have.status(constants.HTTP_UNPROCESSABLE);
          res.body.should.to.be.an('object');
          res.body.should.to.have.property('mensagem');
          done();
        });
    });

    it('Faltando campo senha - POST /users', (done) => {
      chai.request(app)
        .post('/users')
        .send({
          nome: 'Francisco Dias',
          email: 'netoffdf@gmail.com',
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.to.have.status(constants.HTTP_UNPROCESSABLE);
          res.body.should.to.be.an('object');
          res.body.should.to.have.property('mensagem');
          done();
        });
    });

    it('Cadastro Ok - POST /users', (done) => {
      chai.request(app)
        .post('/users')
        .send({
          nome: 'Francisco Dias',
          email: 'netoffdf@gmail.com',
          senha: '123mudar',
          telefones: [{ numero: 999999, ddd: 81 }],
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.have.status(constants.HTTP_OK);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.should.have.property('nome')
            .equal('Francisco Dias');
          res.body.should.have.property('email')
            .equal('netoffdf@gmail.com');
          res.body.should.have.property('token');
          res.body.should.have.property('ultimo_login');
          res.body.should.have.property('data_criacao');
          res.body.should.have.property('data_atualizacao');
          res.body.telefones.every(i => expect(i).to.have.all.keys('ddd', 'numero', '_id'));
          done();
        });
    });
  });

  describe('Login - POST /login', () => {
    it('Email inválido - POST /login', (done) => {
      chai.request(app)
        .post('/login')
        .send({
          email: 'fcodiasgmail.com',
          senha: user.senha,
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.have.status(constants.HTTP_UNPROCESSABLE);
          res.body.should.be.a('object');
          res.body.should.have.property('mensagem');
          done();
        });
    });

    it('Senha errada - POST /login', (done) => {
      chai.request(app)
        .post('/login')
        .send({
          email: user.email,
          senha: 'x',
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          expect(res).to.have.status(constants.HTTP_UNAUTHORIZED);
          res.body.should.be.a('object');
          res.body.should.have.property('mensagem');
          done();
        });
    });

    it('E-mail errado - POST /login', (done) => {
      chai.request(app)
        .post('/login')
        .send({
          email: 'fcodias@gmail.com.br',
          senha: '123mudar',
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.have.status(constants.HTTP_UNAUTHORIZED);
          res.body.should.be.a('object');
          res.body.should.have.property('mensagem');
          done();
        });
    });

    it('Login OK - POST /login', (done) => {
      chai.request(app)
        .post('/login')
        .send({
          email: 'netoffdf@gmail.com',
          senha: '123mudar',
        })
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.have.status(constants.HTTP_OK);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.should.have.property('nome')
            .equal('Francisco Dias');
          res.body.should.have.property('email')
            .equal('netoffdf@gmail.com');
          res.body.should.have.property('token');
          res.body.should.have.property('ultimo_login');
          res.body.should.have.property('data_criacao');
          res.body.should.have.property('data_atualizacao');
          res.body.telefones.every(i => expect(i).to.have.all.keys('ddd', 'numero', '_id'));
          done();
        });
    });
  });

  describe('JWT - GET /me', () => {
    it('Token inválido - GET /me', (done) => {
      chai.request(app)
        .get('/me')
        .set('Authorization', 'Bearer 123invalido')
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.have.status(constants.HTTP_UNAUTHORIZED);
          res.body.should.be.a('object');
          res.body.should.have.property('mensagem');
          done();
        });
    });

    it('Token Válido - GET /me', (done) => {
      chai.request(app)
        .get('/me')
        .set('Authorization', `Bearer ${user.token}`)
        .set('content-type', 'application/json')
        .end((err, res) => {
          res.should.have.status(constants.HTTP_OK);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.should.have.property('nome')
            .equal('Francisco Dias');
          res.body.should.have.property('email')
            .equal('fcodias@gmail.com');
          res.body.should.have.property('token');
          res.body.should.have.property('ultimo_login');
          res.body.should.have.property('data_criacao');
          res.body.should.have.property('data_atualizacao');
          res.body.telefones.every(i => expect(i).to.have.all.keys('ddd', 'numero', '_id'));
          done();
        });
    });
  });
});
