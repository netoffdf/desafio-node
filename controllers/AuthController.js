/* eslint-disable consistent-return */
const { validationResult } = require('express-validator/check');
const User = require('../models/User');

/**
 * POST /login
 */

exports.login = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ mensagem: errors.array()[0].msg });
  }

  const { email, senha } = req.body;

  User.findOne({ email }).then((user) => {
    if (!user || !user.validPassword(senha)) {
      return res.status(401).json({ mensagem: 'Usuário e/ou senha inválidos' });
    }
    return res.status(200).json(user.toAuthJSON());
  }).catch(error => res.status(400).json({ mensagem: error }));
};
