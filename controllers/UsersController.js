/**
 * GET /me
 */

exports.getAuthUser = (req, res) => res.status(200).json(req.user);
