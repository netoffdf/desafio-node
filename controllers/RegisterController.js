/* eslint-disable consistent-return */
const { validationResult } = require('express-validator/check');
const User = require('../models/User');

/**
 * POST /users
 */

exports.insert = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ mensagem: errors.array()[0].msg });
  }

  const {
    nome, email, senha, telefones,
  } = req.body;

  User.create({
    nome, email, senha, telefones,
  }).then(user => res.status(200).json(user.toAuthJSON()))
    .catch(error => res.status(400).json({ mensagem: error.message }));
};
