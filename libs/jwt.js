/* eslint-disable consistent-return */
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const { secret } = require('../config');
const constants = require('../utils/constants');

module.exports = {
  jwt: (req, res, next) => {
    const bearerHeader = req.headers.authorization;

    if (typeof bearerHeader === 'undefined') {
      return res.status(constants.HTTP_FORBIDDEN).send({ mensagem: constants.MSG_FORBIDDEN });
    }
    const tokenArr = bearerHeader.split(' ');
    const token = tokenArr[1];

    if (!token || token === undefined) {
      return res.status(constants.HTTP_FORBIDDEN).send({ mensagem: constants.MSG_FORBIDDEN });
    }


    jwt.verify(token, secret, (err, decode) => {
      if (err) {
        if (err.name === 'TokenExpiredError') {
          return res.status(constants.HTTP_UNAUTHORIZED)
            .send({ mensagem: constants.MSG_INVALIDSESSION });
        }
        return res.status(constants.HTTP_UNAUTHORIZED)
          .send({ mensagem: constants.MSG_UNAUTHORIZED });
      }
      // token válido
      User.findById(decode.id).then((user) => {
        if (user.token === token) {
          req.user = user.toAuthJSON(false);
          return next();
        }
        return res.status(constants.HTTP_UNAUTHORIZED)
          .json({ mensagem: constants.MSG_UNAUTHORIZED });
      }).catch(error => res.status(constants.HTTP_BADREQUEST).json({ mensagem: error }));
    });
  },
};
