# Desafio node.js Concrete Solutions

## Endpoints
- POST /users - Para criar o usuário
- POST /login - Para realizar autenticação
- GET /me - Para realizar a verificação do token

O Projeto está online em https://fcodias-desafio-cs.herokuapp.com/

Antes de rodar o projeto é necessário criar o arquivo .env com as variáveis de ambiente. Existe o `.env.example` como exemplo para criar o novo.

Para rodar o projeto 
```
npm install
npm start
```

Para rodar os testes

```
npm run test
```

Para rodar o lint

```
npm run lint
```

Ao executar o comando `git push` é executado antes os comandos lint e test.

