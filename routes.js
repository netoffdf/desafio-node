const { jwt } = require('./libs/jwt');
const validations = require('./utils/validations');
const registerController = require('./controllers/RegisterController');
const authController = require('./controllers/AuthController');
const usersController = require('./controllers/UsersController');

module.exports = (app) => {
  app.post('/users', validations.register, registerController.insert);
  app.post('/login', validations.login, authController.login);
  app.get('/me', jwt, usersController.getAuthUser);
};
