const { check } = require('express-validator/check');

exports.register = [
  check('email').exists().withMessage('Campo email é obrigatório'),
  check('email').isEmail().withMessage('Insira um e-mail válido'),
  check('nome').exists().withMessage('Campo Nome é obrigatório'),
  check('nome').isLength({ min: 1 }).withMessage('Campo Nome não pode ser em branco'),
  check('senha').exists().withMessage('Campo senha é obrigatório'),
  check('senha').isLength({ min: 1 }).withMessage('Campo Nome não pode ser em branco'),
];
exports.login = [
  check('email').exists().withMessage('Campo email é obrigatório'),
  check('email').isEmail().withMessage('Insira um e-mail válido'),
  check('senha').exists().withMessage('Campo senha é obrigatório'),
  check('senha').isLength({ min: 1 }).withMessage('Campo senha não pode ser em branco'),
];

