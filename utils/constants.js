exports.MSG_UNAUTHORIZED = 'Não autorizado.';
exports.MSG_URLNOTFOUND = 'Rota não encontrada';
exports.MSG_FORBIDDEN = 'O token deve ser enviado pelo Authorization Bearer';
exports.MSG_INVALIDSESSION = 'Sessão inválida';

exports.HTTP_BADREQUEST = 400;
exports.HTTP_UNAUTHORIZED = 401;
exports.HTTP_FORBIDDEN = 403;
exports.HTTP_URLNOTFOUND = 404;
exports.HTTP_OK = 200;
exports.HTTP_UNPROCESSABLE = 422;
